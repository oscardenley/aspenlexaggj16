﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class Spellcaster: MonoBehaviour {

   public static Spellcaster instance;
   public static float       power;
   public static string      damageType;

   [System.Serializable]
   public class Spell {

      public string name, shape, bodyPart;
      public float  power;
      public Color  color;
   }

   public LineRenderer     line;
   public new MeshCollider collider;
   public float            lineTime, minSize;
   public Spell[]          spells;

   public void CastSpellForShape( ShapeMatcher.Shape shape, IEnumerable< Transform > salt, float scale,
                                  Vector3 centrePoint ) {

      if (scale < minSize) { return; }

      Spell spell = null;
      try {
         spell = spells.First( s => s.shape.ToKey() == shape.name.ToKey() );
      } catch {
         Debug.LogWarning( "Couldn't find spell for shape: " + shape );
         return;
      }

      foreach (Transform tf in salt) {

         SpriteRenderer r = tf.GetComponentInChildren< SpriteRenderer >();
         r.color = new Color( spell.color.r, spell.color.g, spell.color.b, r.color.a );
      }

      Vector3[] verts = new Vector3[ shape.points.Count + 1 ];
      for (int i = 0; i < shape.points.Count; i++) {
         verts[ i ] = centrePoint + new Vector3( shape.points[ i ].x, 0.0f, shape.points[ i ].y ) * scale
                        + Vector3.up * 0.2f;
      }
      verts[ shape.points.Count ] = verts[ 0 ];
      line.SetVertexCount( verts.Length );
      line.SetPositions( verts );

      line.enabled = true;
      GameController.Animate( lineTime, x => {
         Color color = Color.Lerp( spell.color, Color.clear, x );
         line.SetColors( color, color );
      }, () => line.enabled = false );

      power      = spell.power / (scale * scale);
      damageType = spell.bodyPart;

      StartCoroutine( PlaceCollider( shape.mesh, centrePoint, scale / shape.scaleFactor ) );
      GameController.instance.PlaySound( SoundType.SPELL_CAST );
   }

   IEnumerator PlaceCollider( Mesh mesh, Vector3 position, float scale ) {
      
      collider.sharedMesh           = mesh;
      collider.transform.position   = position;
      collider.transform.localScale = Vector3.one * scale;
      collider.gameObject.SetActive( true );
      Debug.Log( "Collider is active? " + collider.gameObject.activeSelf );
      yield return new WaitForFixedUpdate();
      yield return new WaitForSeconds( 0.5f );
      collider.gameObject.SetActive( false );
   }
   
   void Awake() {

      instance = this;
      collider.gameObject.SetActive( false );
      line.enabled = false;
   }
   
   void Update() {

   }
}
