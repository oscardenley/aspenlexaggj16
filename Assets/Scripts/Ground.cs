﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;
using System;

public class Ground: MonoBehaviour, IDragHandler, IBeginDragHandler, IEndDragHandler {

   public static bool drawing { get; private set; }

   public void OnBeginDrag( PointerEventData eventData ) {
      
      if (!GameController.instance.gameRunning) { return; }
      drawing = true;
      Salt.StartInput();
   }

   public void OnDrag( PointerEventData eventData ) {
      
      if (!GameController.instance.gameRunning) { return; }
      Salt.Input( eventData.pointerCurrentRaycast.worldPosition );
   }

   public void OnEndDrag( PointerEventData eventData ) {
      
      if (!GameController.instance.gameRunning) { return; }
      drawing = false;
      Salt.EndInput();
   }

   void Start() {

   }
   
   void Update() {

   }
}
