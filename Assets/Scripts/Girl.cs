﻿using UnityEngine;
using System.Collections;

public class Girl: MonoBehaviour {

   public static Girl instance;

   public Sprite         upCasting, rightCasting, downCasting, leftCasting,
                         upIdle,    rightIdle,    downIdle,    leftIdle;
   public SpriteRenderer spriteRenderer;
   public float          startCastingDelay, stopCastingDelay;
   public bool           flipRight;

   bool casting;

   public void StartCasting() {

      Debug.Log("In StartCasting()");

      GameController.DoAfter( startCastingDelay, () => {
         if (casting) { return; }
         casting = true;
         spriteRenderer.sprite = spriteRenderer.sprite.Map( upIdle,    upCasting    )
                                                      .Map( rightIdle, rightCasting )
                                                      .Map( downIdle,  downCasting  )
                                                      .Map( leftIdle,  leftCasting  );
      } );
      if (!casting) {
         GameController.instance.PlaySound( SoundType.SALT_THROW );
         GameController.instance.PlaySound( SoundType.GIRL_GRUNT, 0.5f );
      }
   }

   public void StopCasting() {

      GameController.DoAfter( stopCastingDelay, () => {
         if (!casting) { return; }
         casting = false;
         spriteRenderer.sprite = spriteRenderer.sprite.Map( upCasting,    upIdle    )
                                                      .Map( rightCasting, rightIdle )
                                                      .Map( downCasting,  downIdle  )
                                                      .Map( leftCasting,  leftIdle  );
      } );
   }

   public void SetDirection( int dir ) {

      spriteRenderer.sprite = dir.Map( 0, casting ? upCasting    : upIdle    )
                                 .Map( 1, casting ? rightCasting : rightIdle )
                                 .Map( 2, casting ? downCasting  : downIdle  )
                                 .Map( 3, casting ? leftCasting  : leftIdle  );

      float x = Mathf.Abs( spriteRenderer.transform.localScale.x );
      spriteRenderer.transform.localScale = spriteRenderer.transform.localScale.WithX( dir == 1 && flipRight ? -x : x );
   }
   
   void Awake() {

      instance = this;
   }
   
   void Update() {

   }
}
