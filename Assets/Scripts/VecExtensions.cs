﻿// Last modified: 2016-01-06 - added rotate around pivot

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

  public interface IHasFrequency {

    float frequency { get; set; }
  }
   
  public class DuplicateKeyComparer< TKey >: IComparer< TKey > where TKey: System.IComparable {

  bool descending = false;

  public DuplicateKeyComparer( bool descending=false ) {

    this.descending = descending;
  }

    public int Compare( TKey x, TKey y ) {

      int result = descending ? y.CompareTo( x ) : x.CompareTo( y );
      return result == 0 ? 1 : result;
    }
  }

  class DescendingComparer< T >: IComparer< T > {

    public int Compare( T x, T y ) {
      return Comparer< T >.Default.Compare( y, x );
    }
  }

  public class CappedQueue< T >: Queue< T > {

    int       _maxCount;
    public int MaxCount {
        get { return _maxCount; }
        set {
          _maxCount = value;
          Trim();
        }
    }

    public CappedQueue( int maxCount ): base() { _maxCount = maxCount; }
      
    public new void Enqueue( T item ) {

        base.Enqueue( item );
        Trim();
    }

    void Trim() { while (Count > _maxCount) { Dequeue(); } }
  }

public class ListSet< T >: List< T > {

  bool               addAtEnd;
  Func< T, T, bool > tolerance;
    
  public ListSet( bool addAtEnd=false, Func< T, T, bool > toleranceFunc=null ): base() {

    this.addAtEnd = addAtEnd;
    tolerance     = toleranceFunc;
  }
    
  public ListSet( IEnumerable< T > collection, bool addAtEnd=false,
                  Func< T, T, bool > toleranceFunc=null ): base( collection ) {

    this.addAtEnd = addAtEnd;
    tolerance     = toleranceFunc;
  }
    
  public new void Add( T item ) {

    if (!Contains( item )) {

      if (tolerance == null || Count == 0 || tolerance( this.Last(), item )) { base.Add( item ); }
    }
    else if (addAtEnd) {

      Remove( item );
      base.Add( item );
    }
  }
    
  public new void AddRange( IEnumerable< T > items ) { foreach (T item in items) { Add( item ); } }
}

  public class TimedList< T >: SortedList< float, T > {

    bool          interruptThreads;
    float         lastModifiedTime;

    List< float > intervals;
    float         lastIntervalsRetrievalTime;

    float       _duration;
    public float Duration {
        get { return _duration; }
        set {
          _duration = value;
          Trim();
        }
    }

    public TimedList( float d, bool i=false ): base( new DuplicateKeyComparer< float >() ) {
         
        _duration        = d;
        interruptThreads = i && typeof( T ) == typeof( System.Threading.Thread );
        lastModifiedTime = -1.0f;
    }

    public void Add( T item ) {

        base.Add( Time.time, item );
        lastModifiedTime = Time.time;
        Trim();
    }

    public new void Add( float time, T item ) {

        base.Add( time, item );
        lastModifiedTime = Time.time;
        Trim();
    }

    public void Trim() {

        while (this.Any() && this.First().Key < Time.time - _duration) {

          if (interruptThreads) { ((System.Threading.Thread) (object) this.First().Value).Interrupt(); }
          RemoveAt( 0 );
          lastModifiedTime = Time.time;
        }
    }

    public List< float > Intervals {
        get {
          Trim();
          if (intervals != null && lastModifiedTime == lastIntervalsRetrievalTime) { return intervals; }

          intervals                  = new List< float >();
          lastIntervalsRetrievalTime = lastModifiedTime;
          float lastKey              = -1.0f;

          foreach (float time in Keys) {
              if (lastKey == -1.0f) { lastKey = time; }
              else {
                if (time != lastKey) { intervals.Add( time - lastKey ); }
                lastKey = time;
              }
          }
          return intervals;
        }
    }
  }

public class RotationDamper {

  Transform               tf;
  float                   time;
  TimedList< Quaternion > recent;

  public RotationDamper( Transform transform, float dampTime ) {

    tf     = transform;
    time   = dampTime;
    recent = new TimedList< Quaternion >( dampTime );
  }

  public void Target( Quaternion quaternion ) {

    recent.Add( quaternion );
    tf.rotation = recent.Values.Average();
  }

  public void Reset() {

    recent.Clear();
  }
}

public class GBDictionary< T >: Dictionary< string, T >, IDictionary< string, T > {
    
  public new T this[ string key ] {
    get { return base[ key.ToKey() ]; }
    set { base[ key.ToKey() ] = value; }
  }
    
  public new void Add( string key, T value ) { base.Add( key.ToKey(), value ); }
  public new void Remove( string key ) { base.Remove( key.ToKey() ); }
  public new bool ContainsKey( string key ) { return base.ContainsKey( key.ToKey() ); }

  public GBDictionary(): base() { }

  public GBDictionary( IDictionary< string, T > dict ): base( dict ) {
    foreach (var entry in dict.Keys) { this[ entry ] = dict[ entry ]; }
  }
}

public class CountTable: Dictionary< string, int > {
    
  public new int this[ string key ] {
    get {
      if (!ContainsKey( key )) { this[ key ] = 0; }
      return base[ key ];
    }
    set {
      base[ key ] = value;
    }
  }

  public override string ToString() { return ToString( false ); }

  public string ToString( bool bold=false ) {

    return string.Join( "\n", this.Select( kvp => (bold ? "<b>" : "") + kvp.Key + ": "
                                                    + (bold ? "</b>" : "") + kvp.Value ).ToArray() );
  }
}

public class PeriodicEvaluation< T > {

  public float period;

  Func< T > calculation;
  float     last_evaluation_time;
  T         last_value;

  public PeriodicEvaluation( Func< T > calculation, float period=0.0f ) {

    this.calculation = calculation;
    this.period      = period;
  }

  public T Value {
    get {
      if (Time.time != last_evaluation_time && Time.time >= last_evaluation_time + period) {
        last_evaluation_time = Time.time;
        last_value           = calculation();
      }
      return last_value;
    }
  }
  
  public void SetToReevaluate() {
    last_evaluation_time = Time.time - period * 2.0f;
  }
}

[Serializable]
public class SerializedNullable< T > where T: struct {
  
  [SerializeField] T    value;
  [SerializeField] bool hasValue;

  public T? Value {
    get { return hasValue ? value : (T?) null; }
    set {
      hasValue = value.HasValue;
      if (hasValue) { value = value.Value; }
    }
  }
}

public static class Utils {

  public static bool Overlap( RectTransform a, RectTransform b, RectTransform canvas ) {
      
    Rect a_rect = RectInCanvasSpace( a, canvas );
    Rect b_rect = RectInCanvasSpace( b, canvas );
      
    Vector2 offset  = a_rect.center - b_rect.center;
    bool    overlap = Mathf.Abs( offset.x ) < (a_rect.width  + b_rect.width ) * 0.5f
                   && Mathf.Abs( offset.y ) < (a_rect.height + b_rect.height) * 0.5f;
      
    return overlap;
  }

  public static Rect[] SplitRectHorizontal( Rect rect, float[] division_points, bool proportional=true,
                                            float gap_size=0.0f ) {
    
    Rect[] rects = new Rect[ division_points.Length + 1 ];
    int    i;

    if (proportional) {

      rects[0] = new Rect( rect.x, rect.y, rect.width * division_points[0] - gap_size * 0.5f, rect.height );
      for (i = 0; i < division_points.Length - 1; i++) {
        rects[ i+1 ] = new Rect( rect.x + rect.width * division_points[i] + gap_size * 0.5f, rect.y,
                                 rect.width * (division_points[ i+1 ] - division_points[i]) - gap_size, rect.height );
      }
      rects[ i+1 ] = new Rect( rect.x + rect.width * division_points[i] + gap_size * 0.5f, rect.y,
                               rect.width * (1.0f - division_points[i]) - gap_size * 0.5f, rect.height );
    }
    else {
      rects[0] = new Rect( rect.x, rect.y, division_points[0] - gap_size * 0.5f, rect.height );
      for (i = 0; i < division_points.Length - 1; i++) {
        rects[ i+1 ] = new Rect( rect.x + division_points[i] + gap_size * 0.5f, rect.y,
                                 division_points[ i+1 ] - division_points[i] - gap_size, rect.height );
      }
      rects[ i+1 ] = new Rect( rect.x + division_points[i] + gap_size * 0.5f, rect.y,
                               rect.width - division_points[i] - gap_size * 0.5f, rect.height );
    }
    return rects;
  }

  public static Rect[][] SplitRectLabels( Rect rect, float[] division_points, float label_width ) {

    Rect[]   sections = SplitRectHorizontal( rect, division_points );
    Rect[][] rects    = new Rect[ division_points.Length + 1 ][];

    for (int i = 0; i < division_points.Length + 1; i++) {
      rects[ i ] = SplitRectHorizontal( sections[ i ], new[] { label_width }, false );
    }
    return rects;
  }
    
  public static Rect RectInCanvasSpace( RectTransform r, RectTransform canvas ) {
      
    Vector3[] corners_in_canvas_space = CornersInCanvasSpace( r, canvas );
      
    return new Rect( corners_in_canvas_space[ 0 ].x,  corners_in_canvas_space[ 0 ].y,
                     corners_in_canvas_space[ 2 ].x - corners_in_canvas_space[ 0 ].x,
                     corners_in_canvas_space[ 1 ].y - corners_in_canvas_space[ 0 ].y );
  }
    
  public static Vector3[] CornersInCanvasSpace( RectTransform r, RectTransform canvas ) {
      
    Vector3[] world_corners = new Vector3[ 4 ];
    r.GetWorldCorners( world_corners );
    return world_corners.Select( v => canvas.InverseTransformPoint( v )
                                      + Vector3.up * canvas.rect.height ).ToArray();
  }
  

  public static Vector3 WorldToCanvasSpace( Vector3 world_target, RectTransform canvas_rect,
                                            RectTransform object_transform ) {
    
    Vector3 canvas_point = Camera.main.WorldToViewportPoint( world_target );
    canvas_point.Scale( canvas_rect.rect.size );
    return canvas_point - Vector3.up    * canvas_rect.rect.height * object_transform.anchorMax.y
                        - Vector3.right * canvas_rect.rect.width  * object_transform.anchorMin.x;
  }

  public static Vector2 DistanceFromRectEdge( RectTransform rect_transform, Vector2 local_point ) {

    Vector2 distance = local_point - (Vector2) rect_transform.localPosition;

    for (int i = 0; i < 2; i++) {
      if (distance[i] > 0) {
        distance[i] = Mathf.Max( 0.0f, distance[i] - rect_transform.rect.size[i] * (1.0f - rect_transform.pivot[i]) );
      } else {
        distance[i] = Mathf.Min( 0.0f, distance[i] + rect_transform.rect.size[i] * rect_transform.pivot[i] );
      }
    }
    return distance;
  }

  public static string[] BoldNames< T >( this IEnumerable< T > objects ) where T: UnityEngine.Object {
    return objects.Select( o => o.BoldName() ).ToArray();
  }

  public static string BoldName( this UnityEngine.Object ob ) { return "<b>" + ob.name + "</b>"; }

  public static string StripStyleTags( this string str ) {
    
    foreach (string tag in new[] { "<b>", "</b>", "<i>", "</i>" }) { str = str.Replace( tag, "" ); }
    return str;
  }

  public static string ToKey( this string str ) { return str.ToLower().Replace(" ", ""); }

  public static string CommaJoin( this IEnumerable< string > items, bool oxfordComma=false ) {

    string[]      array = items.ToArray();
    StringBuilder sb    = new StringBuilder();
    string        final = items.Count() > 2 && oxfordComma ? ", and " : " and ";
    for (int i = 0; i < array.Length; i++) {
      if (i == 0)                     { sb.Append(         array[ i ] ); }
      else if (i == array.Length - 1) { sb.Append( final + array[ i ] ); }
      else                            { sb.Append(  ", " + array[ i ] ); }
    }
    return sb.ToString();
  }

  public static string IndefiniteArticle( string thing, bool lowercase=false ) {

    string result = thing != null && thing.Length > 1 && "aeiou".Contains( thing.ToLower()[0] ) ? "An" : "A";
    if (lowercase) { result = "a" + result.Substring( 1 ); }
    return result;
  }

  public static string PrintVals< T >( T[] data ) {
    return string.Join( ", ", data.Select( val => val.GetType().Name + ": " + val.ToString() ).ToArray() );
  }

  public static string PrintVals< TKey, TValue >( IDictionary< TKey, TValue > data,
                                                  bool boldKeys=true, bool boldVals=false ) {

    return string.Join( "\n", data.Select( kvp =>
        (boldKeys ? (kvp.Key.ToString() + ": ").Bold() : (kvp.Key.ToString()   + ": "))
      + (boldVals ?  kvp.Value.ToString().Bold()       :  kvp.Value.ToString()) ).ToArray() );
  }

  struct PrintableValue {

    public string name;
    public string val;
  }
    
  public static PrintValList PrintVals() { return new PrintValList(); }
  public static PrintValList PrintVals< T >( string n, T v ) { return new PrintValList().Add( n, v ); }

  public class PrintValList {

    List< PrintableValue > vals = new List< PrintableValue >();

    public PrintValList Add< T >( string n, T v ) {
        
      string val = v == null ? val = "null" : v.ToString();
      vals.Add( new PrintableValue() { name=n, val=val } );
      return this;
    }

    public string ToString( string separator="\n", bool bold_names=true, bool bold_values=false ) {

      return string.Join( separator, vals.Select( v => (bold_names ? (v.name + ": ").Bold()
                                                                   :  v.name + ": ")
                                            + (bold_values ? (v.val != null ? v.val.ToString() : "null").Bold()
                                                           :  v.val != null ? v.val.ToString() : "null") ).ToArray() );
    }

    public void Log( bool bold_names=true, bool bold_values=false ) {

      Debug.Log( ToString( "\n", bold_names, bold_values ) );
    }
  }

  public class SwitchMap< Tkey, Tresult > {

    Tkey    input;
    Tresult current;

    internal SwitchMap( Tkey input ) { this.input = input; }

    public static implicit operator Tresult( SwitchMap< Tkey, Tresult > map ) { return map.current; }

    public SwitchMap< Tkey, Tresult > Map( Tkey key, Tresult output ) {

      if (input.Equals( key )) { current = output; }
      return this;
    }

    public Tresult Value { get { return current; } }
  }

  public static SwitchMap< T, U > Map< T, U >( this T input, T key, U output ) {

    var dict = new SwitchMap< T, U >( input );
    return dict.Map( key, output );
  }

  public static GBDictionary< T > ToGBDictionary< T >( this IEnumerable< T > collection,
                                                       Func< T, string > keySelector ) {

    GBDictionary< T > dict = new GBDictionary< T >();
    foreach (T item in collection) { dict[ keySelector( item ) ] = item; }
    return dict;
  }

  public static float SeamlessRamp( float x ) { return x - (0.5f / Mathf.PI) * Mathf.Sin( x * 2.0f * Mathf.PI ); }

  public static float ProportionalVariationRange( float x ) { return -( (2.0f * x) / (x - 2.0f) ); }
      
    public static float ReversibleClamp( float value, float bound1, float bound2 ) {
        return Mathf.Clamp( value, Mathf.Min( bound1, bound2 ), Mathf.Max( bound1, bound2 ) );
  }
    
  public static float ValueMap( float input, float inMin, float inMax, float outMin, float outMax ) {
      
    float normalisedInput = (input - inMin) / (inMax - inMin); 
    float result = normalisedInput * (outMax - outMin) + outMin;

    if (Input.GetKey( KeyCode.V )) {

      PrintVals( "input",  input  )
            .Add( "inMin",  inMin  )
            .Add( "inMax",  inMax  )
            .Add( "outMin", outMin )
            .Add( "outMax", outMax )
            .Add( "result", result ).Log();
    }
    return result;

  }
    
  public static float SignedPow( float x, float pow ) { return Mathf.Sign( x ) * Mathf.Pow( Mathf.Abs( x ), pow ); }
    
  public static Color ColorMap( float input, float inMin, float inMax, Color outMin, Color outMax ) {
      
    return new Color( ValueMap( input, inMin, inMax, outMin.r, outMax.r ),
                      ValueMap( input, inMin, inMax, outMin.g, outMax.g ),
                      ValueMap( input, inMin, inMax, outMin.b, outMax.b ),
                      ValueMap( input, inMin, inMax, outMin.a, outMax.a ) );
  }

    public static float Median( this IEnumerable< float > vals ) {

        int n = vals.Count();
        if (n < 1) { throw new ArgumentException("Collection is empty"); }
        List< float > sorted = new List< float >( vals );
        sorted.Sort();
        if (n % 2 == 1) { return sorted[ n / 2 ]; }
        else { return (sorted[ n / 2 - 1 ] + sorted[ n / 2 ]) / 2.0f; }
    }
      
    public static Vector3 Sum( this IEnumerable< Vector3 > vecs ) {

        Vector3 sum = Vector3.zero;
        foreach (Vector3 vec in vecs) { sum += vec; }
        return sum;
    }
      
    public static Vector2 Sum( this IEnumerable< Vector2 > vecs ) {

        Vector2 sum = Vector2.zero;
        foreach (Vector2 vec in vecs) { sum += vec; }
        return sum;
    }

    public static Vector3 Average( this IEnumerable< Vector3 > vecs ) {
         
        if (vecs.Count() < 1) { throw new ArgumentException("Collection is empty"); }
        return vecs.Sum() / vecs.Count();
    }

    public static Vector2 Average( this IEnumerable< Vector2 > vecs ) {
         
        if (vecs.Count() < 1) { throw new ArgumentException("Collection is empty"); }
        return vecs.Sum() / vecs.Count();
    }

    public static string MatrixToString< T >( this IList< IList< T > > vecs ) {

        return string.Join( "\n", vecs.Select( vec => string.Join( ", ", vec.Select( v => v.ToString() )
                                                                            .ToArray() ) )
                                      .ToArray() );
    }
      
    public static T[][] Transpose< T >( this IList< IList< T > > vecs ) {

      return Enumerable.Range( 0, vecs.First().Count() )
                        .Select( i => vecs.Select( v => v[i] )
                                          .ToArray() )
                        .ToArray();
    }

    // Vector median generalised to lists of floats, getting the median of corresponding entries. Filthy.
    public static IEnumerable< float > Median( this IEnumerable< IList< float > > vecs ) {

        return Enumerable.Range( 0, vecs.First().Count() )
                        .Select( i => vecs.Select( v => v[ i ] )
                                          .Median() );
    }

    public static Vector3 Median( this IEnumerable< Vector3 > vecs )  {

        Vector3 vec = Vector3.zero;
        for (int i = 0; i < 3; i++) { vec[i] = vecs.Select( v => v[i] )
                                                  .Median(); }
        return vec;
    }

    public static Vector3 Center( this IEnumerable< Vector3 > vecs ) {

        Vector3 vec = Vector3.zero;
        for (int i = 0; i < 3; i++) { vec[i] = (vecs.Select( v => v[i] ).Min()
                                              + vecs.Select( v => v[i] ).Max()) * 0.5f; }
        return vec;
    }

    public static float AbsoluteSum( this IEnumerable< float > vals ) {

        return vals.Select( v => Mathf.Abs( v ) )
                  .Sum();
    }

    public static Vector3 RandomVector3 {
        get {
          return new Vector3( UnityEngine.Random.value,
                              UnityEngine.Random.value,
                              UnityEngine.Random.value ) * 2.0f - Vector3.one;
        }
    }

    public static Vector2 RandomVector2 {
        get {
          return new Vector2( UnityEngine.Random.value,
                              UnityEngine.Random.value ) * 2.0f - Vector2.one;
        }
    }

    public static T GetOrNull< U, T >( this IDictionary< U, T > dictionary, U key ) where T: class {

        if (!dictionary.ContainsKey( key )) { return null; }
        return dictionary[ key ];
    }

    public static T? FirstOrNull< T >( this ICollection< T > collection, Func< T, bool > predicate ) where T: struct {

    if (collection.Any( i => predicate( i ) )) { return collection.First( i => predicate( i )); }
    return null;
  }

    public static void DestroyGameObject( this UnityEngine.Object o ) {
       
      if (o.GetType().IsSubclassOf( typeof( Component ) )) {
        UnityEngine.Object.Destroy( (o as Component).gameObject );
      } else {
        UnityEngine.Object.Destroy( o );
      }
    }

    public static void DestroyAll< T >( this ICollection< T > collection, bool destroyGameObject=true,
                                        bool clear=true ) where T: UnityEngine.Object {

      foreach (T item in collection) {
        if (destroyGameObject) { item.DestroyGameObject(); }
        else { UnityEngine.Object.Destroy( item ); }
      }
      if (clear) { collection.Clear(); }
    }

    public static void DestroyAllValues< T, U >( this IDictionary< T, U > collection, bool destroyGameObject=true )
                                      where U: UnityEngine.Object {
       
      foreach (U item in collection.Values) {
        if (destroyGameObject) { item.DestroyGameObject(); }
        else { UnityEngine.Object.Destroy( item ); }
      }
      collection.Clear();
    }
    
  public static void Destroy< T >( this ICollection< T > collection, T o, bool destroyGameObject=true )
                        where T: UnityEngine.Object {
      
    collection.Remove( o );
    if (destroyGameObject) { o.DestroyGameObject(); }
    else { UnityEngine.Object.Destroy( o ); }
  }
    
  public static void Destroy< T, U >( this IDictionary< T, U > collection, T key, bool destroyGameObject=true )
                           where U: UnityEngine.Object {

    UnityEngine.Object o = collection.GetOrNull( key );
    if (!o) {
      Debug.LogWarning("Key not found in IDictionary.Destroy()");
      return;
    }
    collection.Remove( key );
    if (destroyGameObject) { o.DestroyGameObject(); }
    else { UnityEngine.Object.Destroy( o ); }
  }

  // modified from http://stackoverflow.com/a/653602
  public static int RemoveAll< TKey, Tvalue >( this SortedList< TKey, Tvalue > collection,
                                            Func< KeyValuePair< TKey, Tvalue >, bool > predicate ) {

    KeyValuePair< TKey, Tvalue > element;
    int                          num_removed = 0;

    for (int i = 0; i < collection.Count; i++) {
        element = collection.ElementAt( i );
        if (predicate( element )) {
            collection.RemoveAt( i );
            num_removed++;
            i--;
        }
    }
    return num_removed;
  }

    public static T Random< T >( this IEnumerable< T > collection ) {

        return collection.ElementAt( UnityEngine.Random.Range( 0, collection.Count() ) );
    }

    // Should only be used on sets of similar quaternions
    public static Quaternion Average( this IEnumerable< Quaternion > quats ) {

        int        n   = 0;
        Quaternion avg = Quaternion.identity;

        foreach( Quaternion q in quats ) {

          avg = Quaternion.Slerp( avg, q, 1.0f / n );
          n++;
        }
        return avg;
    }

    public static void SetAlpha( this Graphic graphic, float alpha ) {
        
      if (!graphic) { return; }
      Color color   = graphic.color;
      color.a       = alpha;
      graphic.color = color;
    }

    public static void SetAlpha( this Material material, float alpha ) {
       
      Color color    = material.color;
      color.a        = alpha;
      material.color = color;
    }

    public static bool IsNullOrWhitespace( this string str ) {

      if (str == null) { return true; }
      if (str.Trim().Length == 0) { return true; }
      return false;
    }

    public static string Color( this string str, Color col ) {
      return "<color=#" + ColorUtility.ToHtmlStringRGBA( col ) + ">" + str + "</color>";
    }

    public static string Bold( this string str ) { return "<b>" + str + "</b>"; }

    public static string Truncated( this string str, int max, string suffix="...", int display_max=-1 ) {

      if (str == null) { return null; }
      if (display_max < 0) { display_max = max; }
      return str.Length > max ? str.Substring( 0, display_max ) + suffix : str;
    }

  public static string MaxSubstring( this string str, int startIndex, int length ) {

    length = Mathf.Clamp( length, 0, str.Length - startIndex );
    return str.Substring( startIndex, length );
  }

    public static T[] EnumValues< T >() {
      return Enum.GetValues( typeof( T ) ).Cast< T >().ToArray();
    }

    public static T ToEnum< T >( this string str ) {
      return (T) Enum.Parse( typeof( T ), str );
    }

    public static float Sqrt( this float f )          { return Mathf.Sqrt( f );    }
    public static float Pow ( this float f, float p ) { return Mathf.Pow ( f, p ); }

  public static int Ring( this int n, int size, int change ) {

    n += change;
    while (n <  0  )  { n += size; }
    while (n >= size) { n -= size; }
    return n;
  }

  public static float Ring( this float n, float size, float change=0.0f ) {

    n += change;
    while (n <  0  )  { n += size; }
    while (n >= size) { n -= size; }
    return n;
  }

  public static float TimeLog( float start_time=-1, string procedure=null ) {
      
    if (start_time >= 0 && procedure != null) {
      Debug.Log( "Time taken for <b>" + procedure + "</b>: " + (Time.realtimeSinceStartup - start_time) );
    }
    return Time.realtimeSinceStartup;
  }

  public static void DoWhenTrue( MonoBehaviour behaviour, Func< bool > condition, Action action,
                                 Func< bool > abort_condition=null ) {

    string trace = Environment.StackTrace;
    string[] split_trace = trace.Split( new[] {'\n'} );
      for (int i = 0; i < split_trace.Length; i++) {
      string[] split_line = split_trace[ i ].Split( (char[]) null, StringSplitOptions.RemoveEmptyEntries );
      for (int j = 0; j < split_line.Length; j++) {
        split_line[ j ] = split_line[ j ].Split( new[] {'\\'} ).Last();
      }
      split_trace[ i ] = string.Join( " ", split_line );
      //split_trace[ i ] = string.Join( " ", new[] { split_line[0], split_line[1], split_line.Last() } );
    }
    trace = string.Join( "\n", split_trace );
    behaviour.StartCoroutine( WaitForCondition( action, condition, behaviour.name + " (" + behaviour.GetType() + ")",
                                                trace, abort_condition ) );
  }

  static IEnumerator WaitForCondition( Action action, Func< bool > condition, string name, string trace,
                                       Func< bool > abort_condition=null ) {

    while (true) {
      if (abort_condition != null && abort_condition()) { yield break; }
      if (!condition()) { yield return null; }
      else { break; }
    }
    //WarlockDebug.Log( "DoWhenTrue action about to be performed on: " + name + ", trace follows:\n" + trace );
    action();
  }

  public static T FindComponent< T >( this Component component, string name ) where T: Component {

    foreach (T c in component.GetComponentsInChildren< T >( true )) {
      if (c.name.ToKey() == name.ToKey()) { return c; }
    }
    return null;
  }

  public static TResult To< TSource, TResult >( this TSource source, Func< TSource, TResult > operation ) {

    return operation( source );
  }

  public static IEnumerable< TResult > SplitTo< TResult >( this string str, char separator,
                                                           Func< string, TResult > operation ) {

    return str.Split( new[] { separator } ).Select( s => operation( s ) );
  }

  public static T MinBy< T, TBy >( this IEnumerable< T > items, Func< T, TBy > selector )
                      where TBy: IComparable {

    T   current_item = items.First();
    TBy current_min  = selector( current_item );
    foreach (T item in items) {
      TBy value = selector( item );
      if (value.CompareTo( current_min ) < 0) {
        current_item = item;
        current_min  = value;
      }
    }
    return current_item;
  }

  public static T MaxBy< T, TBy >( this IEnumerable< T > items, Func< T, TBy > selector )
                      where TBy: IComparable {

    T   current_item = items.First();
    TBy current_max  = selector( current_item );
    foreach (T item in items) {
      TBy value = selector( item );
      if (value.CompareTo( current_max ) > 0) {
        current_item = item;
        current_max  = value;
      }
    }
    return current_item;
  }

  public static void SetNavigationSequence( IEnumerable< Selectable > selectables, bool horizontal,
                                            bool keep_current=true ) {

    Selectable prev = null;
    foreach (Selectable s in selectables) {
      if (!keep_current) { s.SetNavigation( null, null, null, null ); }
      Navigation s_navigation = s.navigation;
      if (prev) {
        Navigation prev_navigation = prev.navigation;
        if (horizontal) {
          prev_navigation.selectOnRight = s;
          s_navigation.selectOnLeft     = prev;
        } else {
          prev_navigation.selectOnDown = s;
          s_navigation.selectOnUp      = prev;
        }
        prev.navigation = prev_navigation;
        s.navigation    = s_navigation;
      }
      prev = s;
    }
    Debug.Log( "Setting " + (horizontal ? "horizontal" : "vertical") + " navigation sequence:\n"
                 + string.Join( "\n", selectables.Select( s => s.name ).ToArray() ) );
  }

  public static void SetNavigation( this Selectable selectable,
                                    Selectable up, Selectable down, Selectable left, Selectable right,
                                    bool keep_current=true ) {

    Navigation navigation = selectable.navigation;

    if (up    != null || !keep_current) { navigation.selectOnUp    = up;    }
    if (down  != null || !keep_current) { navigation.selectOnDown  = down;  }
    if (left  != null || !keep_current) { navigation.selectOnLeft  = left;  }
    if (right != null || !keep_current) { navigation.selectOnRight = right; }

    selectable.navigation = navigation;
  }

  public static void SetNavigationMode( this Selectable selectable, Navigation.Mode mode ) {

    Navigation navigation = selectable.navigation;
    navigation.mode       = mode;
    selectable.navigation = navigation;
  }

  static Selectable ClosestSelectableForDirection( Vector2 origin, Vector2 direction,
                                                   IEnumerable< KeyValuePair< Selectable, RectTransform > > others ) {
    
    var current_closest = others.First();
    foreach (var other in others) {
      if (Vector2.Angle( other.Value.anchoredPosition - origin, direction )
          < Vector2.Angle( current_closest.Value.anchoredPosition - origin, direction )) {
        current_closest = other;
      }
    }
    return Vector2.Angle( current_closest.Value.anchoredPosition - origin, direction ) < 90.0f ? current_closest.Key
                                                                                               : null;
  }

  public static void SetFreeformNavigation( Dictionary< Selectable, RectTransform > elements ) {

    if (elements.Count < 2) {

      Debug.LogWarning("Can't set freeform navigation for less than two elements.");
      return;
    }

    foreach (var element in elements) {

      Navigation navigation     = element.Key.navigation;
      Vector2    this_pos       = element.Value.anchoredPosition;
      var        other_elements = elements.Where( e => e.Key != element.Key );
      navigation.selectOnUp     = ClosestSelectableForDirection( this_pos, Vector2.up,    other_elements );
      navigation.selectOnRight  = ClosestSelectableForDirection( this_pos, Vector2.right, other_elements );
      navigation.selectOnDown   = ClosestSelectableForDirection( this_pos, Vector2.down,  other_elements );
      navigation.selectOnLeft   = ClosestSelectableForDirection( this_pos, Vector2.left,  other_elements );
      element.Key.navigation    = navigation;
    }
  }

  public static bool CanNavigateTo( this Selectable self, Selectable other ) {
    
    if (self.navigation.selectOnUp    == other) { return true; }
    if (self.navigation.selectOnRight == other) { return true; }
    if (self.navigation.selectOnDown  == other) { return true; }
    if (self.navigation.selectOnLeft  == other) { return true; }
    return false;
  }

  public static int LogNearest( float value, List< float > vals ) {

    int   closest_ind      = 0;
    float closest_distance = Mathf.Infinity;
    float log_value        = Mathf.Log( value, 2.0f );

    for (int i = 0; i < vals.Count; i++) {

      float distance = Mathf.Abs( Mathf.Log( vals[ i ], 2.0f ) - log_value );

      if (distance < closest_distance) {

        closest_ind      = i;
        closest_distance = distance;
      }
    }
    return closest_ind;
  }

  public static float FlatToLinearRamp( float value, bool reverse=false, float mix=1.0f ) {

    return value.Pow( reverse ? 2.0f : 0.5f ) * mix + value * (1.0f - mix);
  }

  public static int FractionalChoiceProbability( int choices, float min, float max ) {

    int val = Mathf.Clamp( (int) (UnityEngine.Random.Range( min, max ) * choices), 0, choices - 1 );
    //WarlockDebug.Log( "Fractional choice, n: " + choices + ", min: " + min + ", max: " + max + ", chosen: " + val );
    return val;
  }

  public static T WeightedRandom< T >( this IEnumerable< T > items ) where T: IHasFrequency {

    float randomPoint = UnityEngine.Random.Range( 0.0f, items.Sum( i => i.frequency ) );

    foreach (T item in items) {
      if (randomPoint < item.frequency) { return item; }
      randomPoint -= item.frequency;
    }
    Debug.LogWarning( "THIS SHOULD NEVER HAPPEN: iterated over entire weighted item list; returning last. List contents:\n" + string.Join( "\n", items.Select( i => i.ToString() + "; frequency: " + i.frequency ).ToArray() ) );
    return items.Last();
  }

  public static bool InRange( int value, int min, int max ) {

    if (value >= min && value < max) { return true; }
    return false;
  }
  
  public static uint RotateLeft( this uint value, int count )
  {
      return (value << count) | (value >> (32 - count));
  }

  public static int DiceRoll( int num_dice, int dice_max, int base_result=0 ) {

    return base_result + Enumerable.Range( 0, num_dice - 1 )
                                   .Select( n => UnityEngine.Random.Range( 1, dice_max + 1 ) ).Sum();
  }

  public static float Cycle( float period, bool centerOnZero ) {
    return Cycle( period, 0.0f, 0.0f, centerOnZero );
  }

  public static float Cycle( float period, float startTime=0.0f, float phase=0.0f, bool centerOnZero=false ) {
      
    float raw = Mathf.Sin( ((Time.time - startTime) * 2.0f * Mathf.PI) / period + (phase * Mathf.Deg2Rad) );
    return centerOnZero ? raw : raw * 0.5f + 0.5f;
  }

  public static float deltaTimeAdjustment { get { return Time.deltaTime / 0.0166667f; } }

  public static float SignedProjectionMagnitute( Vector3 vector, Vector3 onNormal, bool proportionalToNormal=false ) {
    
      return Vector3.Project( vector, onNormal ).magnitude
               * Mathf.Sign( Vector3.Dot( vector, onNormal ) )
               * (proportionalToNormal ? onNormal.magnitude : 1.0f);
  }

   public static GameObject InstantiateEffect( GameObject effect, float time, Vector3 position, Quaternion rotation,
                                               Vector3? scale=null ) {
      
      GameObject effect_instance = UnityEngine.Object.Instantiate( effect, position, rotation ) as GameObject;
      if (scale.HasValue) { effect_instance.transform.localScale = scale.Value; }
      GameController.DoAfter( time, () => UnityEngine.Object.Destroy( effect_instance ) );
      return effect_instance;
   }

  public static GameObject InstantiateParticleEffect( GameObject effect, Vector3 position, Quaternion rotation,
                                                      Vector3? scale=null ) {
    
    GameObject effect_instance = UnityEngine.Object.Instantiate( effect, position, rotation ) as GameObject;
    if (scale.HasValue) { effect_instance.transform.localScale = scale.Value; }

    GameController.DoAfter( 
      effect_instance.GetComponentsInChildren< ParticleSystem >().Max( ps => ps.startLifetime ),
      () => UnityEngine.Object.Destroy( effect_instance ) );

    return effect_instance;
  }

  public static void BulletTime( float slow_timescale, float duration, bool relative=false ) {
    
    float prev_timescale = Time.timeScale;
    Time.timeScale       = relative ? Time.timeScale * slow_timescale : slow_timescale;
    GameController.DoAfter( duration, () => Time.timeScale = prev_timescale );
  }

  public static bool ChangeMaterial( this Renderer renderer, string name, Action< Material > change ) {

    return renderer.ChangeMaterial( m => m.name == name, change );
  }

  public static bool ChangeMaterial( this Renderer renderer, Func< Material, bool > selector,
                                     Action< Material > change ) {

    Material[] mats    = renderer.materials;
    bool       changed = false;

    for (int i = 0; i < mats.Length; i++) {

      if (selector( mats[ i ] )) {

        change( mats[ i ] );
        changed = true;
      }
    }
    renderer.materials = mats;
    return changed;
  }

  public static void DrawDebugGraph( IEnumerable< float > vals, Color color, float vertical_scale=1.0f,
                                     float interval=1.0f, Vector3? origin_point=null, float duration=0.0f ) {
    
    Vector3 last   = Vector3.zero;
    Vector3 origin = origin_point ?? Vector3.zero;
    int     i      = 0;

    foreach (float val in vals) {

      Vector3 current = origin + new Vector3( i * interval, val * vertical_scale, 0.0f );
      if (i > 0) { Debug.DrawLine( last, current, color, duration ); }
      last = current;
      i++;
    }
  }

  public static void DrawDebugGraph( IEnumerable< int > vals, Color color, float vertical_scale=1.0f,
                                     float interval=1.0f, Vector3? origin_point=null, float duration=0.0f ) {
    
    Vector3 last   = Vector3.zero;
    Vector3 origin = origin_point ?? Vector3.zero;
    int     i      = 0;

    foreach (int val in vals) {

      Vector3 current = origin + new Vector3( i * interval, val * vertical_scale, 0.0f );
      if (i > 0) { Debug.DrawLine( last, current, color, duration ); }
      last = current;
      i++;
    }
  }

  public static Vector3 RotateAround( this Vector3 v, Vector3 pivot, Vector3 angles ) {
    return Quaternion.Euler( angles ) * (v - pivot) + pivot;
  }
    
  public static Vector2 xy( this Vector3 v ) { return new Vector2( v.x, v.y ); }
  public static Vector2 xz( this Vector3 v ) { return new Vector2( v.x, v.z ); }
  public static Vector2 yz( this Vector3 v ) { return new Vector2( v.y, v.z ); }
    
  public static Vector3 xPart( this Vector3 v ) { return Vector3.right   * v.x; }
  public static Vector3 yPart( this Vector3 v ) { return Vector3.up      * v.y; }
  public static Vector3 zPart( this Vector3 v ) { return Vector3.forward * v.z; }
    
  public static Vector3 ZeroX( this Vector3 v ) { return new Vector3( 0.0f, v.y,  v.z  ); }
  public static Vector3 ZeroY( this Vector3 v ) { return new Vector3( v.x,  0.0f, v.z  ); }
  public static Vector3 ZeroZ( this Vector3 v ) { return new Vector3( v.x,  v.y,  0.0f ); }
    
  public static Vector3 WithX( this Vector3 v, float x ) { return new Vector3( x,   v.y, v.z ); }
  public static Vector3 WithY( this Vector3 v, float y ) { return new Vector3( v.x, y,   v.z ); }
  public static Vector3 WithZ( this Vector3 v, float z ) { return new Vector3( v.x, v.y, z   ); }

   public static void SetVec( ref Vector3 v, float? x, float? y, float? z ) {
      v = new Vector3( x ?? v.x, y ?? v.y, z ?? v.z );
   }

   public static Vector3 x0y( this Vector2 v ) { return new Vector3( v.x, 0.0f, v.y ); }
    
  public static Vector2 Scaled( this Vector2 v, Vector2 scale ) {
    v.Scale( scale );
    return v;
  }
  public static Vector3 Scaled( this Vector3 v, Vector3 scale ) {
    v.Scale( scale );
    return v;
  }

  public static Vector3 MagnitudeAdjusted( this Vector3 v, float adjustment ) {
    return v.normalized * (v.magnitude + adjustment);
  }
}
