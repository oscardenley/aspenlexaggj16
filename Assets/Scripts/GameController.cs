﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Linq;

public enum SoundType {

   MUSIC_INTRO,
   MUSIC_TRANSITION,
   MUSIC_LOOP,
   SALT_LOOP,
   SALT_THROW,
   POP,
   SPLAT,
   GIRL_GRUNT,
   GIRL_HIT,
   GIRL_WIN,
   GIRL_LOSE,
   GIRL_IDLE,
   GIRL_SATISFIED,
   MONSTER_HIT,
   MONSTER_DIE,
   MONSTER_SPAWN,
   CANDLE_SNUFF,
   SPELL_CAST,
   START_GAME,
   AMBIENCE
}

[System.Serializable]
public class SoundCategory {

   public SoundType   soundType;
   public AudioClip[] clips;
   public float       minVol     = 1.0f, maxVol   = 1.0f;
   public float       minPitch   = 1.0f, maxPitch = 1.0f;
   public float       minTimeGap = 0.1f;
   public bool        loop;

   internal float lastPlayedTime;
}

public class GameController: MonoBehaviour {

   public static GameController instance;

   public static Coroutine Animate( float length, System.Action< float > lerpAction,
                                    System.Action completionAction=null ) {

      return instance.StartCoroutine( instance.AnimateCoroutine( length, lerpAction, completionAction ) );
   }

   public static Coroutine DoAfter( float time, System.Action action ) {

      return instance.StartCoroutine( instance.WaitCoroutine( time, action ) );
   }

   IEnumerator AnimateCoroutine( float length, System.Action< float > lerpAction, System.Action completionAction ) {

      float startTime = Time.time;

      while (Time.time < startTime + length) {

         lerpAction( (Time.time - startTime) / length );
         yield return null;
      }
      lerpAction( 1.0f );
      if (completionAction != null) { completionAction(); }
   }

   IEnumerator WaitCoroutine( float time, System.Action action ) {

      yield return new WaitForSeconds( time );
      action();
   }

   public Enemy[]         enemyTypes;
   public Renderer[]      candles;
   public float           startingSpawnRate, spawnRateIncrease, spawnDistance, circleRadius;
   public GameObject      enemyDeath;
   public int             maxHP;
   public GameObject      enemyDeathEffect, popEffect;
   public float           deathEffectBaseSize, enemyDeathBaseRadius;
   public int             enemyDeathPower, directHitDamage;
   public float           startingSaltReserve, saltRegainRate;
   public Sprite          candleOutSprite;
   public AudioSource[]   audioSources;
   public SoundCategory[] sounds;
   public float           transitionPeriod;
   public AnimationCurve  transitionCurve;
   public Transform       titleScreenCamPosition;
   public Image           saltReserveImage;
   public GameObject      gameUI, startButton;
   
   internal float saltReserve;
   internal bool  gameRunning = false;

   float       startTime;
   int         hp;
   AudioClip[] playingClips;
   Vector3     gameplayCamPosition;

   public float gameTime {
      get { return Time.time - startTime; }
   }

   void SpawnEnemy() {

      float angle     = Random.Range( 0.0f, Mathf.PI * 2.0f );
      Enemy enemyType = Utils.WeightedRandom( enemyTypes );
      Instantiate( enemyType, new Vector3( Mathf.Cos( angle ), 0.1f, Mathf.Sin( angle ) ) * spawnDistance,
                   enemyType.transform.rotation );
   }

   public void HitCircle() {

      hp--;

      for (int i = 0; i < candles.Length; i++) {
         if (i >= hp) {
            candles[ i ].FindComponent< SpriteRenderer >("candle_flame_2").enabled = false;
            candles[ i ].FindComponent< SpriteRenderer >("candle_base").sprite     = candleOutSprite;
         }
         //candles[ i ].enabled = i <= hp;
      }
      if (hp == 0) { PlaySound( SoundType.GIRL_LOSE ); }
      else { PlaySound( SoundType.GIRL_HIT ); }
      PlaySound( SoundType.CANDLE_SNUFF );
   }

   void Awake() {

      instance                       = this;
      hp                             = maxHP;
      saltReserve                    = startingSaltReserve;
      playingClips                   = new AudioClip[ audioSources.Length ];
      gameplayCamPosition            = Camera.main.transform.position;
      Camera.main.transform.position = titleScreenCamPosition.position;
      PlaySound( SoundType.MUSIC_INTRO );
   }

   void Start() {

      //PlaySound( SoundType.AMBIENCE );
   }

   public void StartGame() {

      StopSound( SoundType.MUSIC_INTRO );
      PlaySound( SoundType.MUSIC_TRANSITION );

      float musicTransitionLength = 0.0f;
      if (SoundExists( SoundType.MUSIC_TRANSITION )) {
         musicTransitionLength = sounds.First( s => s.soundType == SoundType.MUSIC_TRANSITION ).clips.First().length;
      }
      DoAfter( musicTransitionLength, () => PlaySound( SoundType.MUSIC_LOOP ) );

      startButton.SetActive( false );
      Animate( transitionPeriod, x => {
         Camera.main.transform.position = Vector3.Lerp( titleScreenCamPosition.position, gameplayCamPosition,
                                                        transitionCurve.Evaluate( x ) );
      }, () => {
         gameRunning = true;
         startTime   = Time.time;
         gameUI.SetActive( true );
      } );
   }

   void Update() {

      if (!gameRunning) { return; }

      if (Random.value < (startingSpawnRate + spawnRateIncrease * gameTime) * Time.deltaTime) {
         SpawnEnemy();
      }
      saltReserve                 = Mathf.Min( startingSaltReserve, saltReserve + saltRegainRate * Time.deltaTime );
      saltReserveImage.fillAmount = Mathf.InverseLerp( 0.0f, startingSaltReserve, saltReserve );
   }

   bool SoundExists( SoundType type ) {
      
      if (!sounds.Any( s => s.soundType == type )) {

         Debug.LogWarning( "No sounds for sound type: " + type );
         return false;
      }
      return true;
   }

   bool PlayingSound( SoundType type ) {
      
      if (!SoundExists( type )) { return false; }
      SoundCategory sc = sounds.First( s => s.soundType == type );

      for (int i = 0; i < playingClips.Length; i++) {
         if (sc.clips.Contains( playingClips[ i ] ) && audioSources[ i ].isPlaying) {
            return true;
         }
      }
      return false;
   }

   public void PlaySound( SoundType type, float chanceToPlay=1.0f ) {

      if (Random.value > chanceToPlay || !SoundExists( type )) { return; }
      SoundCategory sc = sounds.First( s => s.soundType == type );
      if (sc.lastPlayedTime > Time.time - sc.minTimeGap) { return; }
      if (sc.loop && PlayingSound( type )) { return; }
      PlaySound( sc.clips.Random(), Random.Range( sc.minVol, sc.maxVol ),
                 Random.Range( sc.minPitch, sc.maxPitch ), sc.loop );
      sc.lastPlayedTime = Time.time;
   }

   public void StopSound( SoundType type ) {
      
      if (!SoundExists( type )) { return; }
      SoundCategory sc = sounds.First( s => s.soundType == type );

      for (int i = 0; i < playingClips.Length; i++) if (sc.clips.Contains( playingClips[ i ] )) {
         audioSources[ i ].Stop();
      }
   }

   public void StopSound( AudioClip clip ) {

      for (int i = 0; i < playingClips.Length; i++) if (playingClips[ i ] == clip) { audioSources[ i ].Stop(); }
   }

   public void PlaySound( AudioClip clip, float volume, float pitch, bool loop ) {

      AudioSource source = audioSources.FirstOrDefault( s => !s.isPlaying );
      if (!source) { return; }

      for (int i = 0; i < audioSources.Length; i++) if (!audioSources[ i ].isPlaying) {
            
         playingClips[ i ]       = clip;
         audioSources[ i ].pitch = pitch;
         audioSources[ i ].loop  = loop;
         audioSources[ i ].PlayOneShot( clip, volume );
         audioSources[ i ].loop  = loop;
         break;
      }
   }
}
