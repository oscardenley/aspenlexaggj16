﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class ShapeMatcher: MonoBehaviour {

   public static ShapeMatcher instance;

   [System.Serializable]
   public class Shape {

      public string    name;
      public Transform pointsParent;
      public float     matchBias;
     
      internal float scaleFactor;

      AnimationCurve       _curve;
      public AnimationCurve curve {
         get {
            if (_curve == null) { _curve = CurveFromPoints( points, ref scaleFactor ); }
            return _curve;
         }
      }

      IList< Vector2 >       _points;
      public IList< Vector2 > points {
         get {
            if (_points == null) { _points = PointsFromTransforms( pointsParent ); }
            return _points;
         }
      }
      
      Mesh       _mesh;
      public Mesh mesh {
         get {
            if (!_mesh) {
               List< int >     triangles = new List< int >();
               List< Vector3 > verts     = new List< Vector3 >();
               verts.Add( Vector3.zero );
               verts.AddRange( points.Select( v => new Vector3( v.x, 0.0f, v.y ) ) );

               for (int i = 0; i < verts.Count; i++) {

                  triangles.Add( 0 );
                  triangles.Add( i == verts.Count - 1 ? 1 : i + 1 );
                  triangles.Add( i );
               }
               _mesh           = new Mesh();
               _mesh.vertices  = verts.ToArray();
               _mesh.triangles = triangles.ToArray();
            }
            return _mesh;
         }
      }
   }

   struct PolarPoint {

      public float angle, magnitude;

      public PolarPoint( float angle, float magnitude ) {

         this.angle     = angle;
         this.magnitude = magnitude;
      }
   }

   public Shape[] shapes;

   public Transform      testInput;
   public AnimationCurve testCurve;

   public static bool Clockwise( IList< Vector2 > points ) {
      
      return Enumerable.Range( 0, points.Count ).Sum( i => {
         int i2 = (i + 1) % points.Count;
         return (points[ i2 ].x - points[ i ].x) * (points[ i2 ].y + points[ i ].y);
      } ) > 0;
   }

   public static IList< Vector2 > PointsFromTransforms( Transform parent ) {
      
      return Enumerable.Range( 0, parent.childCount )
                       .Select( i => parent.GetChild( i ) )
                       .Select( tf => new Vector2( tf.position.x, tf.position.z ) )
                       .ToList();
   }

   public static AnimationCurve CurveFromPoints( IList< Vector2 > input, ref float scale ) {

      Vector2 centrePoint = CentrePoint( input );
      
      var reordered = input.Select( v => v - centrePoint )
                           .Select( v => new PolarPoint( Utils.Ring( Mathf.Atan2( v.y, v.x ) / (Mathf.PI * 2f), 1.0f ),
                                                         v.magnitude ) )
                           .OrderBy( p => p.angle )
                           .Reverse();
      
      float avg        = reordered.Average( p => p.magnitude );
      var   normalized = reordered.Select( p => new PolarPoint( p.angle, p.magnitude / avg ) );
      float zeroPoint  = Mathf.Lerp( normalized.First().magnitude, normalized.Last().magnitude,
                               normalized.First().angle / (normalized.First().angle + 1.0f - normalized.Last().angle) );

      AnimationCurve curve = new AnimationCurve();
      curve.AddKey( 0.0f, zeroPoint );
      foreach (PolarPoint point in normalized) { curve.AddKey( point.angle, point.magnitude ); }
      curve.AddKey( 1.0f, zeroPoint );

      scale = avg;
      return curve;
   }

   public static Vector2 CentrePoint( IList< Vector2 > points ) {

      return new Vector2( 0.5f * (points.Min( v => v.x ) + points.Max( v => v.x )),
                          0.5f * (points.Min( v => v.y ) + points.Max( v => v.y )) );
   }

   public Shape CheckShape( IList< Vector2 > input, ref float scale ) {
      
      if (!Clockwise( input )) { input = input.Reverse().ToList(); }
      AnimationCurve inputCurve = CurveFromPoints( input, ref scale );

      Shape bestShape = null;
      float bestDiff  = Mathf.Infinity;

      foreach (Shape shape in shapes) {

         float numPoints = shape.curve.length + inputCurve.length;
         float diff      = (shape.curve.keys.Select( k => Mathf.Abs( k.value -  inputCurve.Evaluate( k.time ) ) ).Sum()
                           + inputCurve.keys.Select( k => Mathf.Abs( k.value - shape.curve.Evaluate( k.time ) ) ).Sum())
                           * ((1.0f - shape.matchBias) / numPoints);

         if (diff < bestDiff) {

            bestShape = shape;
            bestDiff  = diff;
         }
         Debug.Log( "Difference between input and " + shape.name + ": " + diff );
      }
      return bestShape;
   }
   
   void Awake() {

      instance = this;
   }
   
   void Update() {

      if (Input.GetKeyDown( KeyCode.T )) {

         var   points = PointsFromTransforms( testInput );
         float scale  = 0.0f;
         testCurve    = CurveFromPoints( points, ref scale );
         CheckShape( points, ref scale );
      }
   }

   void OnDrawGizmos() {

      foreach (Shape shape in shapes) {

         for (int i = 0; i < shape.pointsParent.childCount; i++) {

            Gizmos.DrawLine( shape.pointsParent.GetChild( i ).position,
                             shape.pointsParent.GetChild( i.Ring( shape.pointsParent.childCount, 1 ) ).position );
         }
      }
   }
}
