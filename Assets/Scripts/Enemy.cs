﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;

public class Enemy: MonoBehaviour, IPointerClickHandler, IHasFrequency {

   public List< GameObject > parts;
   public float              initialSpeed, speedAtCentre;
   public float              Frequency;

   int           size;
   bool          dying          = false;
   List< Enemy > enemiesInRange = new List< Enemy >();

   public float speed {
      get {
         return Utils.ValueMap( transform.position.magnitude,
                                GameController.instance.circleRadius, GameController.instance.spawnDistance,
                                speedAtCentre, initialSpeed );
      }
   }

   public float frequency {
      get { return Frequency; }
      set { Frequency = value; }
   }

   void Start() {

      size = parts.Count;
      GameController.instance.PlaySound( SoundType.MONSTER_SPAWN );
   }
   
   void Update() {
      
      transform.Translate( -transform.position.normalized * speed * Time.deltaTime, Space.World );

      if (dying) { return; }

      if (transform.position.magnitude < GameController.instance.circleRadius) {

         GameController.instance.HitCircle();
         Die( false );
      }
   }

   public void OnTriggerEnter( Collider col ) {

      //Debug.Log( name + " collided with..." + col.name );
      if (col.tag == "Spell") { Damage( Spellcaster.damageType, Spellcaster.power ); }

      if (col.tag == "Enemy") {

         enemiesInRange.Add( col.GetComponent< Enemy >() );
         if (dying) { col.GetComponent< Enemy >().Damage( null, GameController.instance.enemyDeathPower ); }
      }
   }

   void Die( bool explode ) {

      //Debug.Log( name + " died." );
      if (explode) {
         dying = true;
         Utils.InstantiateEffect( GameController.instance.enemyDeathEffect, 0.583f,
                                  transform.position, transform.rotation,
                                  Vector3.one * (size / GameController.instance.deathEffectBaseSize) );
         GetComponent< CapsuleCollider >().radius = size / GameController.instance.enemyDeathBaseRadius;
         GameController.DoAfter( 0.25f, () => Destroy( gameObject ) );
         GameController.instance.PlaySound( SoundType.SPLAT );
         GameController.DoAfter( 0.5f, () => GameController.instance.PlaySound( SoundType.GIRL_SATISFIED, 0.5f ) );
      }
      else {
         Destroy( gameObject );
      }
      GameController.instance.PlaySound( SoundType.MONSTER_DIE );
   }

   void DestroyPart( GameObject part ) {
      
      Utils.InstantiateEffect( GameController.instance.popEffect, 0.208f,
                                 part.transform.position, part.transform.rotation );
      Destroy( part );
      parts.Remove( part );
      GameController.instance.PlaySound( SoundType.POP );
      GameController.instance.PlaySound( SoundType.MONSTER_HIT );
   }

   public void Damage( string damageType, float damage ) {

      if (dying) { return; }

      foreach (GameObject part in parts.ToArray()) {

         if (damageType.IsNullOrWhitespace() || part.name.ToKey().Contains( damageType.ToKey() )) {

            Debug.Log( name + " is taking damage; type: " + damageType + "; damage: " + damage );

            if (damage >= 1.0f) {

               DestroyPart( part );
               damage -= 1.0f;
            }
            else {
               if (Random.value < damage) { DestroyPart( part ); }
               break;
            }
         }
      }
      //if (parts.Count == 0) { Die( !damageType.IsNullOrWhitespace() ); }
      if (parts.Count == 0) { Die( true ); }
   }

   public void OnPointerClick( PointerEventData eventData ) {
      
      Debug.Log( "Clicked on " + name );
      Salt.instance.ThrowAtEnemy( this );
   }
}
