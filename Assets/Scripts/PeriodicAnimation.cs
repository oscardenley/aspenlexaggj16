﻿using UnityEngine;
using System.Collections;

public class PeriodicAnimation: MonoBehaviour {

   public float period;

   float lastPlayTime = 0.0f;
   
   void Start() {

   }
   
   void Update() {

      if (Time.time > lastPlayTime + period) {

         lastPlayTime = Time.time;
      }
   }
}
