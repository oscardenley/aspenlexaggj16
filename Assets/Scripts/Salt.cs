﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Salt: MonoBehaviour {

   public static Salt instance;

   public float     throwTime, vanishTime, throwScale, clearance, targetSpread, targetTime, targetParticles,
                    startHeight, startXoffset, sizeVariance, opacityVariance;
   public Vector3   arc;
   public Transform saltParticle;

   static List< Transform > particles   = new List< Transform >();
   static List< Vector2 >   inputPoints = new List< Vector2 >();

   static int VectorToQuadrant( Vector2 vector ) {

      return (int) (-Mathf.Atan2( vector.y, vector.x ) / (Mathf.PI * 0.5f) + 1.5f).Ring( 4.0f );
   }

   public void ThrowAtEnemy( Enemy enemy ) {

      if (GameController.instance.saltReserve < 1.0f) { return; }

      Vector3 predictedPosition = enemy.transform.position
         - (enemy.transform.position.normalized * enemy.speed * (throwTime + targetTime * 0.5f));

      for (int i = 0; i < targetParticles; i++) {

         int i2 = i;
         GameController.DoAfter( (targetTime / targetParticles) * i2,
                                 () => ThrowSalt( predictedPosition + Random.insideUnitCircle.x0y() * targetSpread ) );
      }
      GameController.DoAfter( throwTime + targetTime * 0.5f,
                              () => enemy.Damage( null, GameController.instance.directHitDamage ) );

      GameController.instance.saltReserve -= 1.0f;
      Girl.instance.StartCasting();
      GameController.DoAfter( throwTime, () => Girl.instance.StopCasting() );
   }

   public static void Reset() {

      particles   = new List< Transform >();
      inputPoints = new List< Vector2 >();
   }

   static void ClearDust( List< Transform > particlesToClear=null ) {
      
      if (particlesToClear == null) { particlesToClear = particles; }
      foreach (Transform tf in particlesToClear) if (tf) { Destroy( tf.gameObject ); }
      particlesToClear.Clear();
   }

   public static void StartInput() {

      ClearDust();
      inputPoints.Clear();
      Girl.instance.StartCasting();
   }

   static void ThrowSalt( Vector3 target, bool sound=true ) {
      
      int quadrant    = VectorToQuadrant( target.xz() );
      Transform salt  = Instantiate( instance.saltParticle, Vector3.zero, instance.saltParticle.rotation ) as Transform;
      salt.Rotate( Vector3.up, Random.Range( 0.0f, 360.0f ), Space.World );
      salt.localScale = salt.localScale * instance.throwScale
                          * Random.Range( 1.0f - instance.sizeVariance, 1.0f + instance.sizeVariance );
      particles.Add( salt );

      foreach (var sprite in salt.GetComponentsInChildren< SpriteRenderer >()) {

         sprite.sortingOrder = quadrant == 0 ? 0 : 2;
         sprite.color        = new Color( 1.0f, 1.0f, 1.0f, Random.Range( 1.0f - instance.opacityVariance, 1.0f ) );
      }

      Vector3 origin = Vector3.left * (quadrant % 2 == 1 ? quadrant - 2 : 0) * instance.startXoffset;

      GameController.Animate( instance.throwTime,
                              x => salt.position = Vector3.Lerp( origin, target, x )
                                                   + instance.arc * Mathf.Sin( x * Mathf.PI )
                                                   + Vector3.up * instance.clearance
                                                   + Vector3.forward * (1.0f - x) * instance.startHeight,
                              () => {
                                 salt.localScale = instance.saltParticle.localScale
                                          * Random.Range( 1.0f - instance.sizeVariance, 1.0f + instance.sizeVariance );
                                 foreach (var sprite in salt.GetComponentsInChildren< SpriteRenderer >()) {
                                    sprite.sortingOrder = 0;
                                 }
                                 if (sound) { GameController.instance.PlaySound( SoundType.SALT_LOOP ); }
                              } );
      Girl.instance.SetDirection( VectorToQuadrant( target.xz() ) );
   }

   public static void Input( Vector3 position ) {
      
      ThrowSalt( position );
      inputPoints.Add( new Vector2( position.x, position.z ) );
   }

   public static void EndInput() {
      
      var particlesToClear = new List< Transform >( particles );
      GameController.DoAfter( instance.vanishTime, () => { ClearDust( particlesToClear ); } );
      GameController.DoAfter( instance.throwTime, () => GameController.instance.StopSound( SoundType.SALT_LOOP ) );
      Vector2            centrePoint  = ShapeMatcher.CentrePoint( inputPoints );
      float              scale        = 0.0f;
      ShapeMatcher.Shape matchedShape = ShapeMatcher.instance.CheckShape( inputPoints, ref scale );
      Spellcaster.instance.CastSpellForShape( matchedShape, particles, scale,
                                              new Vector3( centrePoint.x, 0.0f, centrePoint.y ) );
      Girl.instance.StopCasting();
   }
   
   void Awake() {

      instance = this;
   }
   
   void Update() {

   }
}
